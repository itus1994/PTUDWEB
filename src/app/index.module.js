(function() {
  'use strict';

  angular
    .module('cv', ['ngAnimate', 'ngCookies', 'ngSanitize', 'ui.router', 'ngMaterial']);

})();
