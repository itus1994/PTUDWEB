(function() {
    'use strict';

    angular
        .module('cv')
         .controller('AppController', ['$mdBottomSheet', '$mdSidenav', AppController ]);


	function AppController( $mdBottomSheet, $mdSidenav) {
  	var prop = this;
	prop.tabs=[
    {
      name: 'PROFILE',
      icon: 'http://www.upsieutoc.com/images/2016/04/25/Contact-icon.png',
      content: [
        {
          title: '',
          descrip: 'Hello, I am Tran Van Duc Designer and Front-end Developer. I have ten years experience as a web/interface designer. I have a love of clean, elegant styling, and I have lots of experience in the production of CSS and (X)HTML for modern websites. I have a reasonable grasp of using JavaScript frameworks, specifically jQuery.'
        },
        {
          title: 'Contact',
          descrip: 'Phone: 01668150363'
        },
        {
          title: '',
          descrip: 'Adress: 147, Nguyen Thi Nho St, Ward 16, District 11, Ho Chi Minh, Viet Nam '
        },
        {
          title: '',
          descrip: 'Email: tranvanducqng@gmail.com'
        },
        {
          title: '',
          descrip: 'Facebook: www.facebook.com/duc.otaku'
        }
      ]
    },
    {
      name: 'SKILL',
      icon: 'http://www.exhsl.com/Image/technical.png',
      content: [
        {
          title: 'Technical',
          descrip: 'HTML/CSS'
        },
        {
          title: '',
          descrip: 'Android'
        },
        {
          title: '',
          descrip: 'C#'
        },
        {
          title: '',
          descrip: 'C/C++'
        },
        {
          title: 'Language',
          descrip: 'English'
        },
        {
          title: 'Other',  
          descrip: 'Team work' 
        }
      ]
    },
    {
      name: 'EDUCATION',
      icon: 'https://www.ushstudent.com/images/education-icon.png',
      content: [
        {
          title: '2013 - present',
          descrip: 'Student in Ho Chi Minh City University of Science.'
        },
        {
          title: '2009 - 2012',
          descrip: 'Sutudent in Son Tinh No 1 Hight School'
        }
      ]
    },
    {
      name: 'WORKS',
      icon: 'http://autobahnperformance.com/wp-content/uploads/2015/06/end-of-warranty-check-up-icon.jpg',
      content: [
        {
          title: 'Experience',
          descrip: 'No experience in practice'
        }
      ]
    }
  ];

  prop.selected = prop.tabs[0];
  
  prop.selectfield = selectfield;
  prop.info = {
    name: 'Trần Văn Đức',
    avatar: "http://www.upsieutoc.com/images/2016/03/08/duc.jpg",
    phone: '01668150363',
    address: '147, Nguyen Thi Nho St, Ward 16, District 11, Ho Chi Minh, Viet Nam ',
    email: 'tranvanducqng@gmail.com',
    facebook: 'www.facebook.com/duc.otaku'
  };
  prop.makeContact = makeContact;
  prop.toggleList   = toggleUsersList;

  function toggleUsersList() {
      $mdSidenav('left').toggle();
    }
  
  /**
    * Select the current avatars
    * @param menuId
    */
  function selectfield(field) {
     prop.selected =  field;
   }
  
  function makeContact(info){
    $mdBottomSheet.show(
    {
      controllerAs     : "vm",
      controller       : [ '$mdBottomSheet', ContactSheetController],
      template: '<md-bottom-sheet class="md-list md-has-header"><md-subheader>Contact <span class="name">{{ vm.user.name }}</span>:</md-subheader><md-list><md-list-item ng-repeat="item in vm.items"><md-button ng-click="vm.contactUser(item)"><md-icon id="padding" md-svg-icon="{{ item.icon_url }}"></md-icon>{{item.name}}</md-button></md-list-item></md-list></md-bottom-sheet>',
      parent           : angular.element(document.getElementById('showBottomSheet'))
    });
    
  function ContactSheetController( $mdBottomSheet ) {
       this.user = info;
       this.items = [
         { name: 'Google',
           icon_url: 'https://rawgit.com/angular/material-start/es5-tutorial/app/assets/svg/google_plus.svg'},
         {
           name: 'Facebook',
           icon_url: 'https://upload.wikimedia.org/wikipedia/commons/c/c2/F_icon.svg'
         }
       ];
       this.contactUser = function(action) {
         $mdBottomSheet.hide();
        if (action.name == "Facebook"){
          window.location="https://www.facebook.com/hung.mai.23395"
        }else if(action.name == "Google"){
          window.location="mailto:ngochung1243@gmail.com";
        }
          
       };
     }
  } 
    }
})();
