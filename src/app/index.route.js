(function() {
    'use strict';

    angular
        .module('cv')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('about', {
                url: '/about',
                templateUrl: 'app/main/main.html',
                controller: 'AppController',
                controllerAs: 'appctrl'
            });

        $urlRouterProvider.otherwise('/about');
    }

})();
